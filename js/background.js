/* global chrome */
const CONTACTS_URL = 'https://contacts.google.com/';

chrome.browserAction.onClicked.addListener(() => {
  chrome.tabs.create({ url: CONTACTS_URL });
});
