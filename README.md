# Gmail Contacts
Adds a Google Contacts shortcut to the new Gmail interface

[![Chrome Web Store](https://img.shields.io/chrome-web-store/v/jgakkffecofibnpbdkkphaminipefbjj.svg)](https://chrome.google.com/webstore/detail/jgakkffecofibnpbdkkphaminipefbjj)
